package ep2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BonusGenerator implements ActionListener {
	
	private static final int DELAY_BONUS = 20000;
	
	private List<Bonus> bonus;
	
	public BonusGenerator() {
		bonus = new ArrayList<Bonus>();
		bonus.add(new Bonus(randomXPosition(),0));
	}
	
	public List<Bonus> getBonus() {
		return bonus;
	}

	public static int getDelay() {
		return DELAY_BONUS;
	}
	
	@Override
    public void actionPerformed(ActionEvent e) {
		generateBonus();
    }
	
	public void generateBonus() {
		bonus.add(new Bonus(randomXPosition(),0));
    }
	
	public int randomXPosition() {
    	Random gerador = new Random();
    	return gerador.nextInt(Game.getWidth());
    }
	
}
