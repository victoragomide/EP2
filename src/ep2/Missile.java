package ep2;

public class Missile extends Sprite {
	
	private static final int SPEED_MISSILE = 2;
	
	public Missile(int x, int y) {
		super(x,y);
		loadImage("images/missile.png");
	}
	
	public void moveFront() {
		y -= SPEED_MISSILE;
		if(y < 0) {
			setVisible(false);
		}
	}
}
