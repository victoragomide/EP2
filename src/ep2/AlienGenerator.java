package ep2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ep2.Map.LEVEL;

public class AlienGenerator implements ActionListener {
	
	private static final int INITIAL_DELAY = 500;
	private LEVEL actual_level;
	
	private List<Alien> aliens;
	
	public AlienGenerator() {
		aliens = new ArrayList<Alien>();
	}
	
	public List<Alien> getAliens() {
		return aliens;
	}

	public int getDelay() {
		return INITIAL_DELAY;
	}

	public void setActualLevel(LEVEL level) {
		actual_level = level;
	}
	
	@Override
    public void actionPerformed(ActionEvent e) {
		generateAliens();
    }
	
	public void generateAliens() {
    	aliens.add(new Alien(randomXPosition(),0,actual_level));
    }
	
	public int randomXPosition() {
    	Random gerador = new Random();
    	return gerador.nextInt(Game.getWidth());
    }
	
}
