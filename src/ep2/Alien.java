package ep2;

import ep2.Map.LEVEL;

public class Alien extends Sprite {
	
	private int speed;
	private int score;

	public Alien(int x, int y, LEVEL level) {
		
		super(x,y);
		
		switch(level) {
			case EASY:
				loadImage("images/alien_EASY.png");
				speed = 1;
				score = 100;
				break;
			case MEDIUM:
				loadImage("images/alien_MEDIUM.png");
				speed = 2;
				score = 500;
				break;
			case HARD:
				loadImage("images/alien_HARD.png");
				speed = 3;
				score = 1000;
		}
		
		
	}
	
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void fall() {
		y += speed;
		if (y > Game.getHeight()) {
			setVisible(false);
		}
	}
}
