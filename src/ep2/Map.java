package ep2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {
	
	public enum LEVEL {
		EASY,
		MEDIUM,
		HARD
	};
	
	private static final int MAX_SCORE_EASY = 25000;
	private static final int MAX_SCORE_MEDIUM = 50000;
	
	private static final int DELAY_MEDIUM = 300;
	private static final int DELAY_HARD = 100;
	
	LEVEL level = LEVEL.EASY;
	
    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final Timer timer_map;
    private final Timer timer_aliens;
    private final Timer timer_bonus;
    
    private final Image background;
    private final Spaceship spaceship;
    private final AlienGenerator gen;
    private final BonusGenerator genb;
    
    private boolean gameOver = false;
    private boolean victory = false;
    
    public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();
        
        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);

        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
        
        gen = new AlienGenerator();
        
        timer_aliens = new Timer(gen.getDelay(), gen);
        timer_aliens.start();
        
        genb = new BonusGenerator();
        
        timer_bonus = new Timer(BonusGenerator.getDelay(), genb);
        timer_bonus.start();
        
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
       
        draw(g);

        Toolkit.getDefaultToolkit().sync();
    }

    private void draw(Graphics g) {
        if(gameOver) {
        	Explosion boom = new Explosion(spaceship.getX(),spaceship.getY());
    		g.drawImage(boom.getImage(), boom.getX(), boom.getY(), this);
        	drawGameOver(g);
        }
        else if(victory) {
        	drawMissionAccomplished(g);
        } 
        else {
        	
	        // Draw spaceship
	        if(spaceship.isVisible()) {
	        	g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
	        }
	        	
	        // Draw missiles
	        List<Missile> missiles = spaceship.getMissiles();
	        for(int i=0; i < missiles.size(); i++) {
	        	Missile m = (Missile) missiles.get(i);
	        	g.drawImage(m.getImage(), m.getX(), m.getY(), this);
	        }
	        
	        // Draw aliens
	        for(int i=0; i < gen.getAliens().size(); i++) {
	        	Alien a = (Alien) gen.getAliens().get(i);
	        	g.drawImage(a.getImage(), a.getX(), a.getY(), this);
	        }
	        
	        // Draw bonus
	        for(int i=0; i < genb.getBonus().size(); i++) {
	        	Bonus b = (Bonus) genb.getBonus().get(i);
	        	g.drawImage(b.getImage(), b.getX(), b.getY(), this);
	        }
	        
	        // Display number of lives
	        drawLives(g);
	        
	        // Display score
	        drawScore(g);
	        
	        // Detects next stage
	        detectsNextStage(g);
	        
	        // Detects missile-alien collision
	        detectsMissileAlienCollision(g);
	        
	        // Detects spaceship-alien collision
	        detectsShipAlienCollision(g);
	        
	        // Detects spaceship-bonus collision
	        detectsShipBonusCollision();
	        
        }
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    	
    	if(!gameOver) {
    		updateSpaceship();
    	}
        updateMissiles();
        updateAliens();
        updateBonus();
        repaint();
        
    }
    
    public void drawLives(Graphics g) {
    	String message = "LIVES = " + spaceship.getLives();
        Font font = new Font("Helvetica", Font.BOLD, 14);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, 5, 15);
    }
    
    public void drawScore(Graphics g) {
    	String message = "SCORE = " + spaceship.getScore();
        Font font = new Font("Helvetica", Font.BOLD, 14);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, 350, 15);
    }
    
    private void drawMissionAccomplished(Graphics g) {

        String message = "MISSION ACCOMPLISHED";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void drawGameOver(Graphics g) {

        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }

    private void updateSpaceship() {
        spaceship.move();
    }
  

    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }
        
    }
    
    public void updateMissiles() {
    	
    	List<Missile> missiles = spaceship.getMissiles();
        
        for(int i=0; i < missiles.size(); i++) {
        	
        	Missile m = (Missile) missiles.get(i);
        	if (m.isVisible()) {
        		m.moveFront();
        	}
        	else {
        		missiles.remove(i);
        	}
        	
        }
        
    }
    
    public void updateAliens() {
    	
    	gen.setActualLevel(level);
    	
    	for(int i=0; i < gen.getAliens().size(); i++) {
        	
        	Alien a = (Alien) gen.getAliens().get(i);
        	if (a.isVisible()) {
        		a.fall();
        	}
        	else {
        		gen.getAliens().remove(i);
        	}
        	
    	}
    	
    }
    
    public void updateBonus() {
    	
    	for(int i=0; i < genb.getBonus().size(); i++) {	
        	Bonus b = (Bonus) genb.getBonus().get(i);
        	if (b.isVisible()) {
        		b.fall();
        	} 
        	else {
        		genb.getBonus().remove(i);
        	}
    	}	
    }
    
    public boolean detectsCollision(Sprite a, Sprite b) {
    	Rectangle a_col = a.getBounds();
    	Rectangle b_col = b.getBounds();
    	if(a_col.intersects(b_col)) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    public void detectsShipAlienCollision(Graphics g) {
    	for(int i=0; i < gen.getAliens().size(); i++) {
    		if(detectsCollision(spaceship, gen.getAliens().get(i))) {
    			solvesShipAlienCollision(i,g);
    		}
    	}
    }
    
    public void solvesShipAlienCollision(int i, Graphics g) {
    	gen.getAliens().get(i).setVisible(false);
    	spaceship.setLives(spaceship.getLives() - 1);
    	if(spaceship.getLives() == 0) {
    		spaceship.setVisible(false);
    		gameOver = true;
    	}
    }
    
    public void detectsMissileAlienCollision(Graphics g) {
    	for(int i=0; i < spaceship.getMissiles().size(); i++) {
    		for(int j=0; j < gen.getAliens().size(); j++) {
    			if(detectsCollision(spaceship.getMissiles().get(i), gen.getAliens().get(j))) {
    				spaceship.setScore(spaceship.getScore() + gen.getAliens().get(j).getScore());
    				spaceship.getMissiles().get(i).setVisible(false);
    				gen.getAliens().get(j).setVisible(false);
    				Explosion boom = new Explosion(gen.getAliens().get(j).getX(),gen.getAliens().get(j).getY());
    				g.drawImage(boom.getImage(), boom.getX(), boom.getY(), this);
        		}
    		}
    	}
    }
    
    public void detectsShipBonusCollision() {
    	for(int i=0; i < genb.getBonus().size(); i++) {
    		if(detectsCollision(spaceship, genb.getBonus().get(i))) {
    			solvesShipBonusCollision(i);
    		}
    	}
    }
    
    public void solvesShipBonusCollision(int i) {
    	spaceship.setScore(spaceship.getScore() + Bonus.getScoreBonus());
    	genb.getBonus().get(i).setVisible(false);
    }
    
    public void detectsNextStage(Graphics g) {
    	if(spaceship.getScore() >= MAX_SCORE_EASY && spaceship.getScore() < MAX_SCORE_MEDIUM) {
    		level = LEVEL.MEDIUM;
    		timer_aliens.setDelay(DELAY_MEDIUM);
    	}
    	else if(spaceship.getScore() >= MAX_SCORE_MEDIUM) {
    		level = LEVEL.HARD;
    		timer_aliens.setDelay(DELAY_HARD);
    	}
    }
}