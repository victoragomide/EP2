package ep2;

public class Bonus extends Sprite {
	
	private static final int SCORE_BONUS = 10000;
	private static final int SPEED_BONUS = 3;
	
	public Bonus(int x, int y) {
		super(x,y);
		loadImage("images/bonus.gif");
	}

	public static int getScoreBonus() {
		return SCORE_BONUS;
	}
	
	public void fall() {
		y += SPEED_BONUS;
		if (y > Game.getHeight()) {
			setVisible(false);
		}
	}	
}
